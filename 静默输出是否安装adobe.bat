@echo off


echo Hostname:
hostname > output.txt

echo.
echo IP Address:
ipconfig | findstr IPv4 >> output.txt
echo Searching for connections on port 823...
netstat -an | findstr "823" >> output.txt
