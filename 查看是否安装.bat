@echo off


echo Hostname:
hostname

echo.
echo IP Address:
ipconfig | findstr IPv4
echo Searching for connections on port 823...
netstat -an | findstr "823"

echo Pausing for 20 seconds...
timeout /t 20 /nobreak > nul
