#!/bin/sh

{
    echo "以下是主机名"
    Hostname
    
    echo "以下是ip地址"
    ifconfig en0 | awk 'NR==5' | cut -c6-22
    
    echo "查询是否安装"
    netstat -an | grep 823
    if [ $? -eq 0 ]; then
        echo "已经安装"
    else
        echo "请确认是否安装成功"
    fi
} > output.txt
